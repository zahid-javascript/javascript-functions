function setUpCounter(val){
    return function counter(){
        return val++;
    }
}
let counter1 = setUpCounter(0);
console.log(counter1());
console.log(counter1());
let counter2 = setUpCounter(10);
console.log(counter2());
console.log(counter2());