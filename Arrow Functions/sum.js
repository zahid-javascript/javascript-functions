function sum(num1,num2){
    return num1 + num2;
};
let result = sum(10,5);
console.log(result);

let sumArrow = (num1,num2) => num1+num2;
let resultArrow = sumArrow(10,7);
console.log(resultArrow);