let message = {
    name:'jhon',
    regularFunction:function(){
        console.log(this);
        console.log(this.name);
    },
    arrowFunction:() => {
        console.log('HI'+ this.name);
        console.log(this);
    }
}
message.regularFunction();
message.arrowFunction();
console.log(this);