function introduction(name,profession){
    console.log(`My name is ${name} and I am a ${profession}.`)
}
introduction('Jhon','Student');
introduction.call(undefined,"James","Artist");
introduction.apply(undefined,["Marry","Lawer"]);